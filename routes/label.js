const express = require('express');
const router = express.Router();

const { getLabel, bulkUpdate } = require('../controllers/labelController');

router.route('/').get(getLabel);
router.route('/bulkUpdate').post(bulkUpdate);


module.exports = router