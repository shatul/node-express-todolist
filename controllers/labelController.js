const Todo = require('../models/todoModel');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');

exports.getLabel = catchAsync(async (req, res) => {
    const allLabel = await Todo.distinct('label')
    if (allLabel) {
        res.status(httpStatus.OK).json({
            success: true,
            data: allLabel,
            message: 'Label Found'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'Label are Not Found'
        })
    }
})

exports.bulkUpdate = catchAsync(async (req, res) => {

    for (const key in req.body) {
        if (Object.hasOwnProperty.call(req.body, key)) {
            const element = req.body[key];
            console.log(element._id, element.label);
            await Todo.findByIdAndUpdate(element._id, { label: element.label })
        }
    }
    const allTasks = await Todo.find({}).sort({ label: 1 })
    if (allTasks) {
        res.status(httpStatus.OK).json({
            success: true,
            data: allTasks,
            message: 'Tasks Found'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'Tasks are Not Found'
        })
    }

})